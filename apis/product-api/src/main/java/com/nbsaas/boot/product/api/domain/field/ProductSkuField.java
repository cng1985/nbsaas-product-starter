package com.nbsaas.boot.product.api.domain.field;


/**
*   字段映射类
*/
public class ProductSkuField  {



    public static final String  product = "product";


    public static final String  price = "price";


    public static final String  name = "name";


    public static final String  stockNum = "stockNum";


    public static final String  discount = "discount";


    public static final String  mealFee = "mealFee";


    public static final String  id = "id";


    public static final String  addDate = "addDate";


    public static final String  spec = "spec";


    public static final String  lastDate = "lastDate";

}