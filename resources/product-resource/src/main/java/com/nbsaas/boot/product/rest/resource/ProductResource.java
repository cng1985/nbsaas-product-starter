package com.nbsaas.boot.product.rest.resource;

import com.nbsaas.boot.jpa.data.core.BaseResource;
import com.nbsaas.boot.product.api.apis.ProductApi;
import com.nbsaas.boot.product.api.domain.request.ProductRequest;
import com.nbsaas.boot.product.api.domain.response.ProductResponse;
import com.nbsaas.boot.product.api.domain.simple.ProductSimple;
import com.nbsaas.boot.product.data.entity.Product;
import com.nbsaas.boot.product.data.repository.ProductRepository;
import com.nbsaas.boot.product.rest.convert.ProductEntityConvert;
import com.nbsaas.boot.product.rest.convert.ProductResponseConvert;
import com.nbsaas.boot.product.rest.convert.ProductSimpleConvert;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.function.Function;

/**
*   业务接口实现
*/
@Transactional
@Service
public class ProductResource extends BaseResource<Product, ProductResponse, ProductSimple, ProductRequest> implements ProductApi {

    @Resource
    private ProductRepository productRepository;

    @Override
    public JpaRepositoryImplementation<Product, Serializable> getJpaRepository() {
        return productRepository;
    }

    @Override
    public Function<Product, ProductSimple> getConvertSimple() {
        return new ProductSimpleConvert();
    }

    @Override
    public Function<ProductRequest, Product> getConvertForm() {
        return new ProductEntityConvert();
    }

    @Override
    public Function<Product, ProductResponse> getConvertResponse() {
    return new ProductResponseConvert();
    }




}


